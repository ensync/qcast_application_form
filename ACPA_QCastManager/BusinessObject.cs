﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Asi.Soa.ClientServices;
using Asi.Soa.Core.DataContracts;

namespace ACPA_QCastManager
{
    public class BusinessObject
    {

        public BusinessObject()
        {

        }

        public class UpdateBusinessObjectRequest
        {

            public string BusinesObjectName { get; set;}
            public List<KeyValuePair<string, object>> Properties { get; set;}
            public string ID { get; set;}
            public string ParentEntity { get; set;}
            public UpdateBusinessObjectRequest()
            {
                this.Properties = new List<KeyValuePair<string, object>>();
            }
        }

        public GenericEntityData Update(UpdateBusinessObjectRequest request)
        {
            EntityManager em = new EntityManager("MANAGER");

            var recordToUpdate = new GenericEntityData(request.BusinesObjectName)
            {
                Identity = new IdentityData(request.BusinesObjectName, request.ID),
                PrimaryParentEntityTypeName = request.ParentEntity,
                PrimaryParentIdentity = new IdentityData(request.ParentEntity, request.ID)
            };

            foreach (var property in request.Properties)
            {
                recordToUpdate.Properties.Add(new GenericPropertyData(property.Key, property.Value));
            }

            recordToUpdate.Properties.Add("ID", request.ID);

            var response = em.Update(recordToUpdate);
            
            if(!response.IsValid)
            {
                throw new Exception("Message from SOA(" + response.ValidationResults.Errors + ")");
            }

            return response.Entity;
        }

        public GenericEntityData Insert(UpdateBusinessObjectRequest request)
        {
            EntityManager em = new EntityManager("MANAGER");

            var recordToInsert = new GenericEntityData(request.BusinesObjectName)
            {
                Identity = new IdentityData(request.BusinesObjectName, request.ID),
                PrimaryParentEntityTypeName = request.ParentEntity,
                PrimaryParentIdentity = new IdentityData(request.ParentEntity, request.ID)
            };

            foreach(var property in request.Properties)
            {
                recordToInsert.Properties.Add(new GenericPropertyData(property.Key, property.Value));
            }

            recordToInsert.Properties.Add("ID", request.ID);

            var response = em.Add(recordToInsert);

            if (!response.IsValid)
            {
                throw new Exception("Message from SOA(" + response.ValidationResults.Errors + ")");
            }

            return response.Entity;

        }


        public FindResultsData Find(string businessOb)


    }
}

