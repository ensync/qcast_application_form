﻿    <%@ Control Language="C#" AutoEventWireup="true" CodeFile="ApplicationForm.ascx.cs" Inherits="ACPA_QCastManager.ApplicationForm" %>

        <style type="text/css">

               .mobileDisplay {display: none;} 

              /* Smartphone Portrait and Landscape */ 
              @media only screen 
                and (min-device-width : 320px) 
                and (max-device-width : 480px){ 
                  .mobileDisplay {display: inline;}
                  .regularDisplay{display: none;}
                   .row{font-size: 10px;} !important
              }

                 @media (max-width: @screen-xs) {
                    body{font-size: 10px;}
                }

                @media (max-width: @screen-sm) {
                    body{font-size: 14px;}
                }


                h5{
                    font-size: 1.4em;
                }       

        </style>

        <div>
            <div id="applicationForm" class="row">
                <form  class="col s12">

                    <h3 class="header">QCast Application Form </h3>



                   <%-- <div class="row">
                        <div class="input-field col s12">
                            <input type="number" id="Invoice_Number" name="capture_values" class="validate">
                            <label for="Invoice_Number">Invoice Number</label>
                        </div>
                    </div>--%>

                    <div class="row">
                        <div class="input-field col s12">
                            <input type="email" id="Send_Inspection_Report_To_Emai" name="capture_values" class="validate">
                            <label class="col-sm-3" data-error="wrong" data-success="right" for="Send_Inspection_Report_To_Emai">Inspection Report Email</label>
                        </div>
                    </div>

                    <!--To do: a bunch of email addresses which have what is the purpose next to them  -->
                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" id="Hours_and_days_of_operation" name="capture_values" class="validate">
                            <label class="col-sm-3" for="Hours_and_days_of_operation">Hours And Days of Operation</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" id="Nearest_airport" name="capture_values" class="validate">
                            <label class="col-sm-3" for="Nearest_airport">Nearest Airport</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" id="Driving_directions_from_airpor" name="capture_values" class="validate">
                            <label class="col-sm-3" for="Driving_directions_from_airpor">Driving Directions from Airport to Plants</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" id="Nearest_hotel" name="capture_values" class="validate">
                            <label class="col-sm-3" for="Nearest_Hotel">Nearest Hotel</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" id="Primary_products_manufactured" name="capture_values" class="validate">
                            <label class="col-sm-3" for="Primary_products_manufactured">Primary Products Manufactured (Please Be Specific)</label>
                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col s12">
                            <input type="text" id="Required_Auditor_PPE_during_th" name="capture_values" class="validate">
                            <label class="col-sm-3" for="Required_Auditor_PPE_during_th">Required Auditor PPE During The Audit Of The Plant</label>
                        </div>
                    </div>


                <!-- Check boxes for products-->
                <h5 class="heading">Production methods (check all that apply): </h5>
                <br>
                <h6>Concrete</h6>
                <p class='col s12'>
                    <label class='col s3'>
                        <input type="checkbox" name="capture_values" id="Dry_Cast" />
                        <span>Dry Cast</span>
                    </label>
                    
            <label class='col s3'>
                <input type="checkbox" name="capture_values" id="Wet_Cast" />
                <span>Wet Cast</span>
            </label>
                   
            <label class='col s3'>
                <input type="checkbox" name="capture_values" id="Packer_Head" />
                <span>PackerHead</span>
            </label>
                   
            <label class='col s3'>
                <input type="checkbox" name="capture_values" id="Spun" />
                <span>Spun</span>
            </label>
                </p>

                    <br>

                <h6>Consolidation</h6>
                <p class='col s12'>
                    <label class='col s3'>
                        <input type="checkbox" name="capture_values" id="Form_Vibrators" />
                        <span>Form vibrators</span>
                    </label>
                    
            <label class='col s3'>
                <input type="checkbox" name="capture_values" id="Stinger_Vibrators" />
                <span>Stinger vibrators</span>
            </label>
                    
            <label class='col s3'>
                <input type="checkbox" name="capture_values" id="Vibration_Tables" />
                <span>Vibration tables</span>
            </label>
                    
            <label class='col s3'>
                <input type="checkbox" name="capture_values" id="Hydraulic_Header" />
                <span>Hydraulic header</span>
            </label>
                </p>

                    <br>

                <h6>Curing</h6>
                <p class='col s12'>
                    <label class='col s3'>
                        <input type="checkbox" name="capture_values" id="Steam_Curing" />
                        <span>Steam curing </span>
                    </label>
                   
            <label class='col s3'>
                <input type="checkbox" name="capture_values" id="Curing_Hoods_Traps" />
                <span>Curing hoods, Tarps</span>
            </label>
                    
            <label class='col s3'>
                <input type="checkbox" name="capture_values" id="Water_Curing" />
                <span>Water curing</span>
            </label>
                   
            <label class='col s3'>
                <input type="checkbox" name="capture_values" id="Other_Curing" />
                <span>Others</span>
            </label>
                </p>

                    <br>

                <h6>Joint Configurations</h6>
                <p class='col s12'>
                    <label class='col s2'>
                        <input type="checkbox" name="capture_values" id="Bell_And_Spigot" />
                        <span>Bell and spigot</span>
                    </label>
                    
            <label class='col s2'>
                <input type="checkbox" name="capture_values" id="Tongue_And_Groove" />
                <span>Tongue and groove</span>
            </label>
                   
            <label class='col s2'>
                <input type="checkbox" name="capture_values" id="Confined_O_Ring" />
                <span>Confined 0-ring</span>
            </label>

            <label class='col s2'>
                <input type="checkbox" name="capture_values" id="SingleOffset" />
                <span>Single offset</span>
            </label>
                   
            <label class='col s2'>
                <input type="checkbox" name="capture_values" id="Steel_End_Ring" />
                <span>Steel end ring</span>
            </label>
                    
            <label class='col s2'>
                <input type="checkbox" name="capture_values" id="Other_Joint_Configurations" />
                <span>Other</span>
            </label>
                </p>

                <br>
                <br>

             <div>
                    <div class="input-field col s12">
                        
                        <label class="col s12 regularDisplay" for="Sanitary_Sewer_Recert_Shutdown">Note: If applying for Sanitary Sewer Recertification, please provide dates of scheduled shutdown for the next 6 months.</label>
                        <label class="col s12 mobileDisplay" for="Sanitary_Sewer_Recert_Shutdown">Shutdown dates (Sanitary Sewer).</label>
						<input type="text" id="Sanitary_Sewer_Recert_Shutdown" name="capture_values" class="validate">
					</div>
                </div>

                <br>

                 <div >
                        <div class="input-field col s12">
                            
                            <label class="col s12 regularDisplay" for="Box_Culvert_Cert_Shutdown">Note: If applying for Box Culvert Certification, please provide dates of scheduled shutdown for the next 6 months.</label>
                            <label class="col s12 mobileDisplay" for="Box_Culvert_Cert_Shutdown">Shutdown dates (Box culvert).</label>
							<input type="text" id="Box_Culvert_Cert_Shutdown" name="capture_values" class="validate">
						</div>
                    </div>
            
                <!-- Dropdowns for Certification Statuses?-->
                <br>
                <br>
                <h5 class="heading">Certification Type </h5>
                <br>
                <div class="switch">
                    <label>
                        <input type="checkbox" id="Full_Plant_Certification">
                        <span class="lever"></span>
                        This is a Full Plant Certification
                    </label>
                </div>
                <br>
                <h7 style="color: red;"> Please check where applicable.</h7>


                <br>
                <br>
                <br>
                <div class="col s12">
                    <label class='col s12'>
                        <input type="checkbox" name="capture_values" id="Storm_Sewer_and_Culvert_Pipe" />
                        <span>Storm Sewer and Culvert Pipe</span>
                    </label>
                    <br>
                    <br>
                    <div class="input-field col s6">
                        <select name="capture_values" id="SSCP_Membership_Status">
                            <option value="" disabled selected>Choose your option</option>
                            <option value="member">Member</option>
                            <option value="nonMember">Non Member</option>
                        </select>
                        <label>Membership Status</label>
                    </div>
                    
					<div class="input-field col s6">
						<select name="capture_values" id="SSCP_Certificiation_Status" class="validate" class="validate" required="" aria-required="true">
							<option value="" disabled selected>Choose your option</option>
							<option value="intialCertification">Initial Certification</option>
							<option value="reCertification">Recertification</option>
							<option value="reAudit">Re-Audit</option>
						</select>
						<label>Certification Status</label>
					</div>
                </div>


                    <br>
                    <br>
                    <br>

                <div class="col s12">
                    <label class='col s12'>
                        <input type="checkbox" name="capture_values" id="Sanitary_Sewer" />
                        <span>Sanitary Sewer</span>
                    </label>
                    <br>
                    <br>
                    <div class="input-field col s6">
                        <select id="SS_Membership_Status" name="capture_values">
                            <option value="" disabled selected>Choose your option</option>
                            <option value="member">Member</option>
                            <option value="nonMember">Non Member</option>
                        </select>
                        <label>Membership Status</label>
                    </div>
                    
					<div class="input-field col s6">
						<select id="SS_Certification_Status" name="capture_values">
							<option value="" disabled selected>Choose your option</option>
							<option value="intialCertification">Initial Certification</option>
							<option value="reCertification">Recertification</option>
							<option value="reAudit">Re-Audit</option>
						</select>
						<label>Certification Status</label>
					</div>
                </div>

                <br>
                <br>
                <br>
                <div class="col s12">
                    <label class='col s12'>
                        <input type="checkbox" name="capture_values" id="Box_Culvert_Precast_Structures" />
                        <span>Box Culvert/Three-sided Precast Structures</span>
                    </label>
                    <br>
                    <br>
                    <div class="input-field col s6">
                        <select id="Box_Culvert_Membership_Status" name="capture_values">
                            <option value="" disabled selected>Choose your option</option>
                            <option value="member">Member</option>
                            <option value="nonMember">Non Member</option>
                        </select>
                        <label>Membership Status</label>
                    </div>
                    
					<div class="input-field col s6">
						<select id="Box_Culvert_Certification_Stat" name="capture_values">
							<option value="" disabled selected>Choose your option</option>
							<option value="intialCertification">Initial Certification</option>
							<option value="reCertification">Recertification</option>
							
						</select>
						<label>Certification Status</label>
					</div>
                </div>

                <br>
                <br>
                <br>
                <div class="col s12">
                    <label class='col s12'>
                        <input type="checkbox" name="capture_values" id="Manholes" />
                        <span>Manholes</span>
                    </label>
                    <br>
                    <br>
                    <div class="input-field col s6">
                        <select id="Manholes_Membership_Status" name="capture_values">
                            <option value="" disabled selected>Choose your option</option>
                            <option value="member">Member</option>
                            <option value="nonMember">Non Member</option>
                        </select>
                        <label>Membership Status</label>
                    </div>
                   
					<div class="input-field col s6">
						<select id="Manholes_Certification_Status" name="capture_values">
							<option value="" disabled selected>Choose your option</option>
							<option value="initialCertification">Initial Certification</option>
							<option value="reCertification">Recertification</option>
							
						</select>
						<label>Certification Status</label>
					</div>
                </div>


                <br>
                <br>
                <br>
                <div class="col s12">
                    <label class='col s12'>
                        <input type="checkbox" name="capture_values" id="Sanitary_Manholes" />
                        <span>Sanitary Manholes</span>
                    </label>
                    <br>
                    <br>
                    <div class="input-field col s6">
                        <select id="SManholes_Membership_Status" name="capture_values">
                            <option value="">Choose your option</option>
                            <option value="member">Member</option>
                            <option value="nonMember">Non Member</option>
                        </select>
                        <label>Membership Status</label>
                    </div>
                  
					<div class="input-field col s6">
						<select id="SManholes_Certification_Status" name="capture_values" required="required" aria-required="true">
							<option value="" disabled selected>Choose your option</option>
							<option value="initialCertification">Initial Certification</option>
							<option value="reCertification">Recertification</option>
							
						</select>
						<label>Certification Status</label>
					</div>
                </div>


                <br>
                <br>
                <br>
                <div class="col s12">
                    <label class='col s12'>
                        <input type="checkbox" name="capture_values" id="Other_Concrete_Products" />
                        <span>Other Precast Concrete Products</span>
                    </label>
                    <br>
                    <br>
                    <div class="input-field col s6">
                        <select id="Other_Prod_Membership_Status" name="capture_values">
                            <option value="" disabled selected>Choose your option</option>
                            <option value="member">Member</option>
                            <option value="nonMember">Non Member</option>
                        </select>
                        <label>Membership Status</label>
                    </div>
                   
					<div class="input-field col s6">
						<select id="Other_Prod_Cert_Status" name="capture_values">
							<option value="" disabled selected>Choose your option</option>
							<option value="initialCertification">Initial Certification</option>
							<option value="reCertification">Recertification</option>
							
						</select>
						<label>Certification Status</label>
					</div>
                </div>

                    <br>
                    <br>
                    <br>

                    <h7 class="col s12">*All inspections after the first year are unannounced. The auditing firm requires a schedule of shutdowns in order to conduct inspections when plants are operating. All unscheduled shutdowns shall be reported to both the ACPA and the auditing firm to avoid unnecessary fees.</h7>

                    <br>
                    <br>
                    <br>
                    <br>
                    <br>

                <label class='col s2'>
                <input type="checkbox" name="capture_values" style="float: left" id="Application_Completed" />
                <span>Complete</span>
            </label>

                    <a class="waves-effect waves-light btn-large right-align submit" name="action" style="float: right;background-color:#D77B27">Submit</a>

                </form>

           

            </div>
             <div class="container hidden" id="applicaitonSuccess">
                    <h2>Your application has been submitted successfully</h2>
                    </div>
        </div>

    <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css"  media="screen,projection">



    <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.2/jquery.min.js"></script>-->

    <script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.2.min.js"></script>
    <!-- Compiled and minified JavaScript -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
    <script>
        $(document).ready(function ($) {

            $('select').formSelect();
            var applicationObject = {};

            var readOnlyMode = false;
            var updateApplicationMode = false;
            var insertWithPreviousApplicationDataMode = false;

            var getParameterByName = function (name) {
                name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"), results = regex.exec(location.search);
                return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
            }

            if (getParameterByName('SEQN') && getParameterByName('V') === "RO")
                readOnlyMode = true;
            else if (getParameterByName('SEQN') && getParameterByName('V') === "U")
                updateApplicationMode = true;
            else if (getParameterByName('SEQN') && getParameterByName('V') === "IWPD")
                insertWithPreviousApplicationDataMode = true;

            if (readOnlyMode) {

                $('.submit').addClass('hide');

                $("input[name='capture_values']").map(function () {
                    //if (this.type !== "checkbox")
                    if (this.type !== "checkbox" && this.type !== "select-one")
                        $('#' + this.id).prop("readonly", true);
                    else if (this.type === "checkbox")
                        $('#' + this.id).prop('disabled', true);
                });

                $("select[name='capture_values']").map(function () {
                    var instance = M.FormSelect.getInstance($('#' + this.id));
                    instance.dropdown.el.disabled = true;
                });
            }

            $('#MainBody').height('3250');


      //  $(".switch").find("input[type=checkbox]").on("change", function () {
        //    var status = $(this).prop('checked');
          //  if (status) {
			//		$("#Storm_Sewer_and_Culvert_Pipe").prop("checked", true);
              //      $("#Sanitary_Sewer").prop("checked", true);
                //    $("#Box_Culvert_Precast_Structures").prop("checked", true);
                  //  $("#Manholes").prop("checked", true);
                    //$("#Sanitary_Manholes").prop("checked", true);
                    //$("#Other_Concrete_Products").prop("checked", true);
            //}
           // else {
			//		$("#Storm_Sewer_and_Culvert_Pipe").prop("checked", false);
              //      $("#Sanitary_Sewer").prop("checked", false);
                //    $("#Box_Culvert_Precast_Structures").prop("checked", false);
                  //  $("#Manholes").prop("checked", false);
                    //$("#Sanitary_Manholes").prop("checked", false);
                    //$("#Other_Concrete_Products").prop("checked", false);
            //}
        //});



            var getParameterByName = function (name) {
                name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"), results = regex.exec(location.search);
                return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
            }



            var GetApplicationInformation = function () {

                var dataToSend = JSON.stringify({
                    'seqnNumber': getParameterByName('SEQN')
                })

                $.ajax({
                    type: "POST",
                    url: "http://members.concretepipe.org/qdev/Online/bridge.aspx/GetApplicationInformation",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: dataToSend,
                    success: function (response) {
                        console.log("success", response.d);
                        applicationObject = response.d;
                        $.each(response.d, function (key, value) {
                            if (key !== "__type" && $('#' + key)[0] !== undefined && $('#' + key)[0] !== null) {
                                //console.log("key- ", key , $('#' + key)[0].type);
                                if ($('#' + key)[0].type !== 'checkbox' && $('#' + key)[0].type !== 'select-one' && key !== 'ID' && value !== '') {
                                    $("#" + key).val(value);
                                    $('label[for="' + key + '"]').addClass('active');
                                }
                                else if ($('#' + key)[0].type === 'checkbox' && key !== 'ID' && value !== '') {
                                    if (value === "True")
                                        $("#" + key).prop("checked", true);
                                    else if (value = "False")
                                        $("#" + key).prop("checked", false);
                                }
                                else if ($('#' + key)[0].type === 'select-one' && key !== 'ID' && value !== '') {
                                    console.log("key dropdown", key);
                                    // $("#" + key).value(value);
                                    // $("#" + key).find('option[value='+ value +']').prop('selected', true);
                                    //$("#" + key).addClass('active');
                                    var instance = M.FormSelect.getInstance($('#' + key));
                                    instance.dropdown.el.value = value;
                                    //console.log("instance - ", instance);
                                }
                                if (key === "Application_Completed" && value === "True") {
                                    $("input[name='capture_values']").map(function () {
                                        //if (this.type !== "checkbox")
                                        if (this.type !== "checkbox" && this.type !== "select-one")
                                            $('#' + this.id).prop("readonly", true);
                                        else if (this.type === "checkbox")
                                            $('#' + this.id).prop('disabled', true);
                                    });

                                    $("select[name='capture_values']").map(function () {
                                        var instance = M.FormSelect.getInstance($('#' + this.id));
                                        instance.dropdown.el.disabled = true;
                                    });
                                }
                            }



                        });


                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        var obj = JSON.parse(jqXHR.responseText);
                        console.log(obj.Message);
                        console.log(jqXHR.statusText);
                    }
                });
            }

            if (getParameterByName('SEQN'))
                GetApplicationInformation();



            var checkBoxSSCP = $('#Storm_Sewer_and_Culvert_Pipe');//.querySelector('input[type="checkbox"]');
            var selectSSCP = $('#SSCP_Certificiation_Status');//document.querySelector('input[type="text"]');
            var checkBoxSS = $('#Sanitary_Sewer');
            var selectSS = $('#SS_Certification_Status');
            var checkboxBCC = $('#Box_Culvert_Precast_Structures');
            var selectBCC = $('#Box_Culvert_Certification_Stat');
            var checkboxMH = $('#Manholes');
            var selectMH = $('#Manholes_Certification_Status');
            var checkboxSM = $('#Sanitary_Manholes');
            var selectSM = $('#SManholes_Certification_Status');
            var checkboxOther = $('#Other_Concrete_Products');
            var selectOther = $('#Other_Prod_Cert_Status');
            

            function toggleRequired(checkBox, select) {

                if (select.attr('required') !== 'required') {
                    select.attr('required', 'required');
                    select.attr('aria-required', 'true');

                    $('select[required]').css({
                        display: 'inline',
                        position: 'absolute',
                        float: 'left',
                        padding: 0,
                        margin: 0,
                        border: '1px solid rgba(255,255,255,0)',
                        height: 0,
                        width: 0,
                        top: '2em',
                        left: '3em',
                        opacity: 0
                    });

                }

                else {
                    console.log('values ', checkBox, select);
                    select.attr('required', '');
                    select.attr('aria-required', 'false');
                }
            }
		
			checkBoxSSCP.change(function(){
				toggleRequired(checkBoxSSCP, selectSSCP);
			 });;

            checkBoxSS.change(function(){
				toggleRequired(checkBoxSS, selectSS);
			 });;

            checkboxBCC.change(function(){
				toggleRequired(checkboxBCC, selectBCC);
			 });;

            checkboxMH.change(function(){
				toggleRequired(checkboxMH, selectMH);
			 });;


            checkboxSM.change(function(){
				toggleRequired(checkboxSM, selectSM);
            });;


             checkboxOther.change(function(){
				toggleRequired(checkboxOther, selectOther);
			 });;


            $('.submit').on('click', function () {

                

                $("html, body").animate({
                    scrollTop: 0
                }, 1000);

                //var applicationObject = {};

                $("input[name='capture_values']").map(function () {
                    if (this.type !== "checkbox")
                        applicationObject[this.id] = this.value;
                    else
                        applicationObject[this.id] = this.checked;

                });

                if (getParameterByName('ID') !== null)
                    applicationObject["ID"] = getParameterByName('ID');

                $("select[name='capture_values']").map(function () {
                    applicationObject[this.id] = this.value;
                });

                //console.log(applicationObject);



                if (updateApplicationMode)
                    UpdateApplicationData(applicationObject, getParameterByName('SEQN'));



                InsertApplicationData(applicationObject)

            });


            var InsertApplicationData = function (appObject) {
                var dataToSend = JSON.stringify({
                    applicationFormObject: appObject
                });

                $.ajax({
                    type: "POST",
                    url: "http://members.concretepipe.org/qdev/Online/bridge.aspx/InsertPlantCert",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: dataToSend,
                    success: function (response) {
                        $('#applicationForm').addClass('hidden');
                        $('#applicaitonSuccess').removeClass('hidden');
                        setTimeout(function () { window.location = "http://members.concretepipe.org/QDev/Online/QCast_Application/Select_QCast_Manager.aspx?ID=" + getParameterByName('ID'); }, 2000);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        var obj = JSON.parse(jqXHR.responseText);
                        console.log(obj.Message);
                        console.log(jqXHR.statusText);
                        //  toastr.error(obj.Message + jqXHR.statusText);
                    }
                });
            }

            var UpdateApplicationData = function (appObject, seqnNumber) {
                var dataToSend = JSON.stringify({
                    applicationFormObject: appObject,
                    seqnNumber: seqnNumber
                });
                //
                $.ajax({
                    type: "POST",
                    url: "http://members.concretepipe.org/qdev/Online/bridge.aspx/UpdatePlantCert",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: dataToSend,
                    success: function (response) {
                        $('#applicationForm').addClass('hidden');
                        $('#applicaitonSuccess').removeClass('hidden');
                        setTimeout(function () { window.location = "http://members.concretepipe.org/QDev/Online/QCast_Application/Select_QCast_Manager.aspx?ID=" + getParameterByName('ID'); }, 2000);
                    },
                    error: function (jqXHR, textStatus, errorThrown) {
                        var obj = JSON.parse(jqXHR.responseText);
                        console.log(obj.Message);
                        console.log(jqXHR.statusText);
                        //  toastr.error(obj.Message + jqXHR.statusText);
                    }
                });
            }

        });
    </script>

