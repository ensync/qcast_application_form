﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FileUploader.ascx.cs" Inherits="ACPA_QCastManager.FileUploader" %>


<div>  
    <input type="file" name="UploadFile" id="txtUploadFile" />
</div> 

<%--<body>
  <div class="file_input_div">
    <div class="file_input">
      <label class="image_input_button mdl-button mdl-js-button mdl-button--fab mdl-button--mini-fab mdl-js-ripple-effect mdl-button--colored">
        <i class="material-icons">file_upload</i>
        <input id="file_input_file" class="none" type="file" />
      </label>
    </div>
    <div id="file_input_text_div" class="mdl-textfield mdl-js-textfield textfield-demo">
      <input class="file_input_text mdl-textfield__input" type="text" disabled readonly id="file_input_text" />
      <label class="mdl-textfield__label" for="file_input_text"></label>
    </div>
  </div>
</body>--%>

<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/css/materialize.min.css"  media="screen,projection">


<script src="http://ajax.aspnetcdn.com/ajax/jQuery/jquery-2.1.2.min.js"></script>
<!-- Compiled and minified JavaScript -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
<script>
    $(document).ready(function ($) {


        ////define upload fields

        //$()


        ////on click of upload field, pop up explorer window



        //$('.upload-area').on('click', function (e) {
        //    var id = $(this).attr('data-sort');
        //    $('#upload-' + id).replaceWith($('#upload-' + id).clone());
        //    $('#upload-' + id).one('change', function (e) { fileSelect(e); });
        //    $('#upload-' + id).trigger('click');
        //});

        $('#txtUploadFile').on('change', function (e) {
            var files = e.target.files;
            //var myID = 3; //uncomment this to make sure the ajax URL works
            if (files.length > 0) {
                if (window.FormData !== undefined) {
                    var data = new FormData();
                    for (var x = 0; x < files.length; x++) {
                        data.append("file" + x, files[x]);
                    }

                    $.ajax({
                        type: "POST",
                        url: '/MyController/UploadFile?id=' + myID,
                        contentType: false,
                        processData: false,
                        data: data,
                        success: function (result) {
                            console.log(result);
                        },
                        error: function (xhr, status, p3, p4) {
                            var err = "Error " + " " + status + " " + p3 + " " + p4;
                            if (xhr.responseText && xhr.responseText[0] == "{")
                                err = JSON.parse(xhr.responseText).Message;
                            console.log(err);
                        }
                    });
                } else {
                    alert("This browser doesn't support HTML5 file uploads!");
                }
            }
        });

    });

</script>