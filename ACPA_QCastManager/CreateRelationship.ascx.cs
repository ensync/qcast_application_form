﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;


    public partial class CreateRelationship : System.Web.UI.UserControl
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string companyID = GetIDFromURL("COID");
            string ID = GetIDFromURL("ID");
            //string loggedInUserID = Asi.Security.Utility.SecurityHelper.GetSelectedImisId();
            bool successFlag = AddRelationship(companyID, ID, "QMGR", "QPLANT");
            if(successFlag)
            {
                Response.Redirect("QCast_Plant_Manager_Confirmation.aspx?ID="+ID+"&COID="+companyID);
            }

        }

        public string GetIDFromURL(string parameterName)
        {
            return Request.QueryString[parameterName];
        }


        //establishes relationship between company ID and user ID
        public static bool AddRelationship(string CompanyID, string loggedInUserID, string relationship, string reciprocal)
        {
            bool returnVal = false;

            if (!string.IsNullOrEmpty(CompanyID) && !string.IsNullOrEmpty(loggedInUserID))
            {
                returnVal = true;
                int counter = 0;
               

                //check if relationship exists before adding relationship
                if (!CheckRelationshipExists(CompanyID, loggedInUserID, relationship))
                {
                    string connectionString = WebConfigurationManager.ConnectionStrings["DataSource.iMIS.Connection"].ConnectionString;
                    using (SqlConnection conn = new SqlConnection(connectionString))
                    {
                        try
                        {
                            conn.Open();
                            //get last value from relationship counter 
                            SqlCommand cmd = new SqlCommand();
                            cmd.Connection = conn;
                            cmd.CommandType = System.Data.CommandType.StoredProcedure;
                            cmd.CommandText = "sp_asi_GetCounter";

                            SqlParameter param = new SqlParameter("@counterName", SqlDbType.VarChar);
                            param.Value = "Relationship";
                            param.Direction = ParameterDirection.Input;
                            cmd.Parameters.Add(param);

                            SqlDataReader counterReader = cmd.ExecuteReader(); 
                            while (counterReader.Read())
                            {
                                if (counterReader["LAST_VALUE"] != null)
                                {
                                    counter = Int32.Parse(counterReader["LAST_VALUE"].ToString());
                                }
                            }

                            //if counter exists, insert relationship into relationship table with counter value

                            if (counter > 0)
                            {
                                string gradeQuery = "INSERT INTO Relationship(ID, RELATION_TYPE, TARGET_ID, TARGET_RELATION_TYPE, STATUS, DATE_ADDED, UPDATED_BY, SEQN) VALUES ( '" + CompanyID + "', '" + relationship + "', '" + loggedInUserID + "', '" + reciprocal + "', 'A', '" + DateTime.Now + "', '" + Asi.Security.Utility.SecurityHelper.GetSelectedImisId() + "', '" + counter + "')"; //" +  + "
                                SqlCommand command = new SqlCommand(gradeQuery, conn);

                                SqlDataReader relationshipReader = command.ExecuteReader();
                                if (relationshipReader.RecordsAffected > 0)
                                {
                                    returnVal = true;
                                }
                            }
                        }
                        catch (Exception err)
                        {
                            throw new Exception(counter + err.Message + " || " + err.StackTrace);
                        }
                        finally
                        {
                            conn.Close();
                        }
                    }

                }
                return returnVal;
            }
            
            else
                throw new Exception("Empty parameters passed");
        }


        public static bool CheckRelationshipExists(string ID, string TargetID, string Relationship)
        {
            int counter = 0; bool returnVal = false;
            string connectionString = WebConfigurationManager.ConnectionStrings["DataSource.iMIS.Connection"].ConnectionString;
            using (SqlConnection conn = new SqlConnection(connectionString))
            {
                try
                {
                    conn.Open();
                    SqlCommand cmd = new SqlCommand("select count(*) as COUNTER from Relationship where ID = @ID and RELATION_TYPE = @RELATION_TYPE and TARGET_ID = @TARGET_ID", conn);
                    cmd.Parameters.AddWithValue("@ID", ID);
                    cmd.Parameters.AddWithValue("@TARGET_ID", TargetID);
                    cmd.Parameters.AddWithValue("@RELATION_TYPE", Relationship);

                    SqlDataReader relReader = cmd.ExecuteReader();

                    while (relReader.Read())
                    {
                        if (relReader["COUNTER"] != DBNull.Value)
                            counter = (int)relReader["COUNTER"];
                        if (counter > 0)
                        {
                            returnVal = true;
                        }
                    }
                }
                catch (Exception err)
                {
                    throw new Exception(err.Message + " || " + err.StackTrace);
                }
                finally
                {
                    conn.Close();
                }
            }
            return returnVal;
        }


    }

