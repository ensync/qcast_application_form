﻿using Asi.Data;
using Asi.Soa.ClientServices;
using Asi.Soa.Core.DataContracts;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Reflection;
using System.IO;


namespace ACPA_QCastManager
{

    public partial class Bridge : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        public class ApplicationObject
        {
            public string ID {get; set;}
            public string Plant_Name { get; set; }
            //public string AddressLineOne { get; set; }
            //public string AddressLineTwo { get; set; }
            public string Hours_and_days_of_operation { get; set; }
            public string Nearest_airport { get; set; }
            public string Nearest_hotel { get; set; }
            public string Driving_directions_from_airpor { get; set; }
            //
            //public string Invoice_Number { get; set; }
            public string Send_Invoice_To_Name { get; set; }
            public string Send_Invoice_To_Email { get; set; }
            public string Send_Inspection_Report_To_Emai { get; set; }
            public string Primary_products_manufactured { get; set; }
            public string Receive_Report { get; set; }
            //
            public string Required_Auditor_PPE_during_th { get; set; }
            //
            public string Dry_Cast { get; set; }
            public string Wet_Cast { get; set; }
            public string Packer_Head { get; set; }
            public string Spun { get; set; }
            //
            public string Form_Vibrators { get; set; }
            public string Stinger_Vibrators{ get; set; }
            public string Vibration_Tables{ get; set; }
            public string Hydraulic_Header { get; set; }
            //
            public string Steam_Curing { get; set; }
            public string Curing_Hoods_Traps { get; set; }
            public string Water_Curing { get; set; }
            public string Other_Curing { get; set; }
            //
            public string Bell_And_Spigot { get; set; }
            public string Tongue_And_Groove { get; set; }
            public string Confined_O_Ring { get; set; }
            public string SingleOffset { get; set; }
            public string Steel_End_Ring { get; set; }
            public string Other_Joint_Configurations { get; set; }
            //
            public string Full_Plant_Certification { get; set; }
            public string StormSewerAndCulvertPipeMemberStatus { get; set; }

            public string Storm_Sewer_and_Culvert_Pipe { get; set; }
            public string Sanitary_Sewer { get; set; }
            public string Box_Culvert_Precast_Structures { get; set; }
            public string Manholes { get; set; }
            public string Sanitary_Manholes { get; set; }
            public string Other_Concrete_Products{ get; set; }

            public string SSCP_Membership_Status { get; set; }
            public string SSCP_Certificiation_Status { get; set; }
            public string SS_Membership_Status { get; set; }
            public string SS_Certification_Status { get; set; }
            public string Box_Culvert_Membership_Status { get; set; }
            public string Box_Culvert_Certification_Status { get; set; }

            public string Manholes_Membership_Status { get; set; }
            public string Manholes_Certification_Status { get; set; }
            public string SManholes_Certification_Status { get; set; }
            public string Other_Prod_Membership_Status { get; set; }
            public string SManholes_Membership_Status { get; set; }
            public string Other_Prod_Cert_Status { get; set; }
            public string Application_Completed { get;set;}

            public string Application_Date { get; set; }
            public string Applicant_ID { get;set; }

        }

        [WebMethod]
        public static ApplicationObject GetApplicationInformation(string seqnNumber)
        {
            ApplicationObject obj = new ApplicationObject();
            EntityManager em = new EntityManager("MANAGER");
            try
            {
                QueryData businessObjectQuery = new QueryData("QCAST_Name_Plant_Cert");
                businessObjectQuery.AddCriteria(new CriteriaData("SEQN", OperationData.Equal, seqnNumber));
                FindResultsData qcastTableResults = em.Find(businessObjectQuery);
                if (qcastTableResults != null && qcastTableResults.Result != null && qcastTableResults.Result.Count > 0)
                {
                   foreach (GenericEntityData qcastResult in qcastTableResults.Result)
                    {
                      foreach (string propertyName in qcastResult.GetPropertyNames())
                        {
                           try
                            {
                                PropertyInfo prop = obj.GetType().GetProperty(propertyName, BindingFlags.Public | BindingFlags.Instance);
                                if (null != prop && prop.CanWrite && qcastResult["\"" + propertyName + "\""] != null)
                                {
                                    prop.SetValue(obj, qcastResult[propertyName].ToString(), null);
                                }
                            }
                            catch(Exception error)
                            {
                                throw new Exception("Error - " + error.GetBaseException() + " - " + error.Message + " - " + error.InnerException + " - " + error.Source + " - " + error.StackTrace);
                            }
                        }

                    }


                }
            }
            catch (Exception error)
            {
                throw new Exception("Error - " + error.Message);
            }

            return obj;
        }

        [WebMethod]
        public static void InsertPlantCert(ApplicationObject applicationFormObject)
        {
            ApplicationObject responseObject = new ApplicationObject();

            try
            {

                EntityManager em = new EntityManager("MANAGER");

                GenericEntityData applicationRequestObject = new GenericEntityData();

                Type type = applicationFormObject.GetType();
                PropertyInfo[] properties = type.GetProperties();
                applicationRequestObject.EntityTypeName = "Table:Name_Plant_Cert";
                applicationRequestObject["PartyId"] = applicationFormObject.ID;
                applicationFormObject.Applicant_ID = Asi.Security.Utility.SecurityHelper.GetSelectedImisId();
                applicationFormObject.Application_Date = DateTime.Now.ToString("M/d/yyyy");
                foreach (PropertyInfo property in properties)
                {
                    applicationRequestObject[property.Name] = property.GetValue(applicationFormObject, null);
                }

               

                ValidateResultsData<GenericEntityData> updateResults;
                updateResults = em.Add(applicationRequestObject);
                foreach (ValidationResultData vrd in updateResults.ValidationResults.Errors)
                {
                    throw new Exception("Error - " + vrd.Message);
                }
            } 
            catch (Exception error)
            {
                throw new Exception("Error - " + error.Message + error.StackTrace);
            }
        }

      
        //update using SOA
        [WebMethod]
        public static void UpdatePlantCert(ApplicationObject applicationFormObject, string seqnNumber)
        {
            try
            {

                EntityManager em = new EntityManager("MANAGER");
                GenericEntityData applicationRequestObject = new GenericEntityData();
                Type type = applicationFormObject.GetType();
                PropertyInfo[] properties = type.GetProperties();
                applicationRequestObject.EntityTypeName = "Table:Name_Plant_Cert";
                applicationRequestObject["PartyId"] = applicationFormObject.ID;
                applicationRequestObject["SEQN"] = seqnNumber;
                foreach (PropertyInfo property in properties)
                {
                    applicationRequestObject[property.Name] = property.GetValue(applicationFormObject, null);
                }
                ValidateResultsData<GenericEntityData> updateResults;
                updateResults = em.Update(applicationRequestObject);
                foreach (ValidationResultData vrd in updateResults.ValidationResults.Errors)
                {
                    throw new Exception("Error - " + vrd.Message);
                }
            }
            catch (Exception error)
            {
                throw new Exception("Error - " + error.Message + error.StackTrace);
            }
        }


        [WebMethod]
        public string UploadHomeReport(string id)
        {
            try
            {
                foreach (string file in Request.Files)
                {
                    var fileContent = Request.Files[file];
                    if (fileContent != null && fileContent.ContentLength > 0)
                    {
                        // get a stream
                        var stream = fileContent.InputStream;
                        // and optionally write the file to disk
                        var fileName = Path.GetFileName(file);
                        var path = Path.Combine(Server.MapPath("~/App_Data/Images"), fileName);
                        using (var fileStream = File.Create(path))
                        {
                            stream.CopyTo(fileStream);
                        }
                    }
                }
            }
            catch (Exception)
            {
                //Response.StatusCode = (int)HttpStatusCode.BadRequest;
                return "Upload failed";
            }

            return "File uploaded successfully";
        }

     }
    
}